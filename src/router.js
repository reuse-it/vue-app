import VueRouter from 'vue-router'
import Vue from 'vue'

import AddProductPage from './pages/AddProductPage';
import MainPage from './pages/MainPage';
import LoginPage from './pages/LoginPage';
import ProfilePage from './pages/ProfilePage';
import SearchProductPage from './pages/SearchProductPage';
import ProductPage from './pages/ProductPage';
import Error404Page from './pages/Error404Page';
//TODO: Implement jwt authorization for routing
const routes = [
    { path: '/', component: MainPage },
    { path: '/login', component: LoginPage },
    { path: '/profile/:username', component: ProfilePage },
    { path: '/addproduct', component: AddProductPage },
    { path: '/products', component: SearchProductPage },
    { path: '/products/:productid', component: ProductPage },
    { path: '*', component: Error404Page }
];

const router = new VueRouter({
    routes
});

router.beforeResolve((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        let user;
        Vue.prototype.$Amplify.Auth.currentAuthenticatedUser().then(data => {
            if (data && data.signInUserSession) {
                user = data;
            }
            next()
        }).catch(() => {
            next({
                path: '/'
            });
        });
    }
    next()
})

export default router
